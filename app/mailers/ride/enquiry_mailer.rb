module Ride
  class EnquiryMailer < ActionMailer::Base
    default :from => "from@example.com"

    def notify(enquiry)

      @enquiry = enquiry

      @to = Ride.notification_email
      @from = @enquiry.name + ' <' + @enquiry.email + '>'
      @subject = 'New enquiry at ' + Ride.admin_title

      mail(:to => @to, :from => @from, :subject => @subject)

    end

  end
end
