module Ride
  class HomePageImage < Ride::Base::Model

    
    ride :display_in_admin => true, :admin_title => 'Home Page Images'
    
    has_attached_file :image,
      :styles => {

        :thumb => "245x54#",
        :large => '980x216#'

      },

      :storage => :s3,
      :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
      :path => "/jimmydavis/home_page_images/:id/:attachment/:style.:basename.:extension",
      :bucket => 'uploads.managemybands.com',
      :s3_permissions => :public_read   

  end
end
