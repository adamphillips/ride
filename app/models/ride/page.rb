module Ride
  class Page < Base::Model

    ride :display_in_admin => true

    validates :title, :presence => true
    validates :url, :presence => true

  end
end
