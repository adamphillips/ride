module Ride
  class HomePagePost < Ride::Base::Model

    
    ride :display_in_admin => false, :admin_title => 'Home Page Posts'
    
    default_scope :order => 'created_at desc'

  end
end
