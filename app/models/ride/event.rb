module Ride
  class Event < Base::Model

    ride :display_in_admin => true

    default_scope :order => 'date desc'

    validates :title, :presence => true
    validates :date, :presence => true
    
    has_attached_file :image,
      :styles => {

        :thumb => "50x50>",
        :large => '300x400>'
      },

      :storage => :s3,
      :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
      :path => "/jimmydavis/events/:id/:attachment/:style.:basename.:extension",
      :bucket => 'uploads.managemybands.com',
      :s3_permissions => :public_read    

  end
end
