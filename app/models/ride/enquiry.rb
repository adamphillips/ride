module Ride
  class Enquiry < Ride::Base::Model

    ride :display_in_admin => true

    default_scope :order => 'created_at desc'    

    def title

      self.name + ' - ' + self.created_at.to_date.to_s(:long_ordinal)

    end

  end
end
