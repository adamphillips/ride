#require 'media_links/photo'
#require 'media_links/sound_cloud_player'
#require 'media_links/you_tube_video'

module Ride
  class MediaLink < Ride::Base::Model

    ride :display_in_admin => true 

    has_attached_file :image,
      :styles => {

        :thumb => "130x96#",
        :mid => "260x192#",
        :large => '520x384#'
      },

      :storage => :s3,
      :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
      :path => "/jimmydavis/products/:id/:attachment/:style.:basename.:extension",
      :bucket => 'uploads.managemybands.com',
      :s3_permissions => :public_read   

    def subclasses
      ret = super
      ret.push(Ride::MediaLinks::Photo) if !ret.include?(Ride::MediaLinks::Photo)  
      ret.push(Ride::MediaLinks::SoundCloudPlayer) if !ret.include?(Ride::MediaLinks::SoundCloudPlayer)  
      ret.push(Ride::MediaLinks::YouTubeVideo) if !ret.include?(Ride::MediaLinks::YouTubeVideo)  

      ret 
    end

    def type_class
      self.class.type_class
    end

    def type_title
      self.class.type_title
    end


    # Makes sure that url_for still works ok
    def self.inherited(child)
      child.instance_eval do
        def model_name
          Ride::MediaLink.model_name
        end
      end
      super
    end

    # Generates a list of type options based on the sub classes
    def self.select_options
      subclasses.map{ |c| [c.type_title, c.to_s] }.sort
    end

    def self.type_class
      'media_link'
    end

    def self.type_title
      'Media link'
    end

  end
end

%w[you_tube_video sound_cloud_player photo].each do |c|
  ##debugger
  require File.join(Ride::Engine.root.to_s, "app", "models", "ride", "media_links", "#{c}.rb")
end

#require Ride::MediaLinks::Photo

