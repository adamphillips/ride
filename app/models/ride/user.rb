module Ride
  class User < ActiveRecord::Base

    # Include default devise modules. Others available are:
    # :token_authenticatable, :lockable, :timeoutable and :activatable,:registerable, :confirmable, :omniauthable, 
    devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable    

  end
end
