module Ride
  class Ability

    include CanCan::Ability

    def initialize(user=nil)

      #debugger

      user ||= Ride::User.new

      @user = user
      
      if @user && @user.id

        can :manage, :all

      end
      
    end

  end
end
