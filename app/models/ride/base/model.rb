module Ride

  module Base

    class Model < ActiveRecord::Base

      cattr_reader :per_page
      @@per_page = 5

      def singular_variable_name
        self.class.name.split('::').last.to_s.singularize
      end

    end

  end
  
end
