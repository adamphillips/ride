module Ride
  module MediaLinks
    class Photo < MediaLink
      def self.type_class
        'photo'
      end

      def self.type_title
        'Photo'
      end
    end
  end
end
