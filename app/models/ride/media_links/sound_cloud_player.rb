module Ride
  module MediaLinks
    class SoundCloudPlayer < MediaLink
      def self.type_class
        'music'
      end

      def self.type_title
        'Music'
      end
    end
  end
end
