module Ride
  module MediaLinks
    class YouTubeVideo < MediaLink
      def self.type_class
        'video'
      end

      def self.type_title
        'Video'
      end
    end
  end
end

