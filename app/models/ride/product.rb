module Ride
  class Product < Ride::Base::Model

    ride :display_in_admin => true   

    has_attached_file :image,
      :styles => {

        :thumb => "150x150#",
        :large => '450x450#'
      },

      :storage => :s3,
      :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
      :path => "/jimmydavis/products/:id/:attachment/:style.:basename.:extension",
      :bucket => 'uploads.managemybands.com',
      :s3_permissions => :public_read    

  end
end
