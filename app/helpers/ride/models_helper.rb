module Ride

  # Contains model helper methods
  module ModelsHelper

    def self.included(base)

      base.extend ClassMethods
  
    end

    module ClassMethods

      def ride(options={})

        #debugger

        #set default options
        options[:display_in_admin] ||= false

        options[:admin_title] ||= self.name.split('::').last.to_s.humanize.pluralize
        options[:admin_link] ||= {:controller => self.name.underscore.pluralize.gsub('::', '/'), :action => 'index'}
        options[:table] ||= 'ride_' + self.name.split('::').last.to_s.underscore.pluralize

        #debugger

        Ride.admin_models[self.name] = self if ((options[:display_in_admin] == true)&&(!Ride.admin_models[self.name]))

        cattr_accessor :ride_options
        self.ride_options = options

        set_table_name options[:table]

        #debugger        

        include Ride::ModelsHelper::InstanceMethods
        extend Ride::ModelsHelper::StaticMethods

      end

    end

    module StaticMethods
    end

    module InstanceMethods
    end

  end

end
