module Ride

  module ControllersHelper

      def self.included(base)

        #debugger

        base.extend ClassMethods
        
      end

      def create

        resource = self.class.model.new(params[self.class.singular_variable_name.to_sym])

        #debugger

        if resource.save

          flash[:notice] = 'Record saved'
          
          instance_variable_set '@' + self.class.singular_variable_name, resource

          redirect_to :action => 'index'

        else

          instance_variable_set '@' + self.class.singular_variable_name, resource

          render :action => 'new'

        end

      end

      def destroy 

        resource = self.class.model.find(params[:id])

        if resource.delete

          instance_variable_set '@' + self.class.singular_variable_name, resource

          flash[:notice] = 'Record has been deleted'

          redirect_to :action => 'index'

        end

      end
    
      def edit

        instance_variable_set '@' + self.class.singular_variable_name, self.class.model.find(params[:id])

      end

      # Basic crud methods      
      def index

        variable_name = '@' + self.class.plural_variable_name
        variable = instance_variable_get variable_name

        if paginate

          #debugger

          if variable
            instance_variable_set variable_name, variable.paginate(:page => params[:page])
          else
            instance_variable_set variable_name, self.class.model.paginate(:page => params[:page])
          end
        
        else
          instance_variable_set '@' + self.class.plural_variable_name, self.class.model.all if !instance_variable_get '@' + self.class.plural_variable_name
        end

      end

      def new

        instance_variable_set '@' + self.class.singular_variable_name, self.class.model.new

      end

      def show

        #debugger

        resource = self.class.model.find(params[:id])

        instance_variable_set '@' + self.class.singular_variable_name, resource

      end

      def update

        resource = self.class.model.find(params[:id])

        if resource.update_attributes(params[self.class.singular_variable_name])

          flash[:notice] = 'Record saved'

          instance_variable_set '@' + self.class.singular_variable_name, resource

          if params[self.class.singular_variable_name] && params[self.class.singular_variable_name][:type]
            eval '@' + self.class.singular_variable_name + '.type = "' + params[self.class.singular_variable_name][:type] + '"'
            eval '@' + self.class.singular_variable_name + '.save'
          end

          #debugger

          redirect_to :action => 'index'

        else

          instance_variable_set '@' + self.class.singular_variable_name, resource
           
          render :action => 'edit'

        end

      end

   

      #Class methods

      module ClassMethods

        def model
          #@@model = self.model_name.constantize
          self.model_name.constantize
        end

        def model_name
          #@@model_name ||= self.name.to_s.gsub('Controller', '').singularize
          self.name.to_s.gsub('Controller', '').singularize
        end

        def plural_variable_name
          #@@plural_variable_name ||= self.model_name.split('::').last.to_s.pluralize.underscore
          self.model_name.split('::').last.to_s.pluralize.underscore
        end

        def singular_variable_name
          #@@singular_variable_name ||= self.model_name.split('::').last.to_s.singularize.underscore
          self.model_name.split('::').last.to_s.singularize.underscore
        end

      end

  end

end
