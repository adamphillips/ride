module Ride
  module ApplicationHelper
  end

  class FormBuilder < ActionView::Helpers::FormBuilder

      def field(method, options = {})

        options[:label] ||= method.to_s.humanize.capitalize
        options[:type] ||= 'text_field'

        return '<p><label for="' + @object_name + '_' + method.to_s + '">' + options[:label] + '</label>' + self.send(options[:type], method, options) + '</p>'

      end

      def multiple_select(method, choices, options = {}, html_options = {})

        html_options[:multiple] = 'multiple'
        html_options[:size] = 8 if !html_options[:size]
        
        return select(method, choices, options, html_options)
      end

      def select_field(method, choices, options = {}, html_options = {})
        options[:label] ||= method.to_s.humanize.capitalize

        return '<p><label for="' + @object_name + '_' + method.to_s + '">' + options[:label] + '</label>' + select(method, choices, options, html_options)
 + '</p>'

      end

      private

      def field_name(label,index=nil)
        output = index ? "[#{index}]" : ''
        return @object_name + output + "[#{label}]"
      end

      def field_id(label,index=nil)
        output = index ? "_#{index}" : ''
        return @object_name + output + "_#{label}"
      end

  end
    
end
