module Ride
  class MediaLinksController < Ride::Base::RideController

    #load_and_authorize_resource :class => Ride::MediaLink

    before_filter do
      Ride::MediaLinks::Photo.new
      Ride::MediaLinks::SoundCloudPlayer.new
      Ride::MediaLinks::YouTubeVideo.new
    end

    def create

      #debugger

      if params[:media_link] && params[:media_link][:type]
        resource = params[:media_link][:type].constantize.new(params[:media_link])
      else
        resource = self.class.model.new(params[self.class.singular_variable_name.to_sym])
      end

      #debugger

      if resource.save

        flash[:notice] = 'Record saved'
        
        instance_variable_set '@' + self.class.singular_variable_name, resource

        redirect_to :action => 'show', :id => resource.id

      end

    end

  end
end
