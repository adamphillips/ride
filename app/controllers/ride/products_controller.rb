module Ride
  class ProductsController < Ride::Base::RideController

    load_and_authorize_resource :class => Ride::Product    

  end
end
