module Ride
  class SessionsController < Devise::SessionsController

    def create

      respond_to do |format|

        @user = User.find_by_email(params[:user][:email])

        #debugger

        if (@user)
          
          sign_in(@user)

          #format.mobile {redirect_to({:controller => 'static', :action => 'home'})}
          format.html {redirect_to({:controller => 'static', :action => 'home'})}

        else

          flash[:error] = 'Could not find a match for that email address'

          redirect_to({:action => 'new'})

        end

      end

    end

    def new

      #render :layout => 'sign_in'

      #debugger
      
      @user = User.new

      render :layout => 'ride/sign_in'

    end

  end
end
