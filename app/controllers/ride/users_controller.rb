module Ride

  class UsersController < ApplicationController

    load_and_authorize_resource :class => Ride::User

    def index

      @users = Ride::User.all      

    end

  end

end
