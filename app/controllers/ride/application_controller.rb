module Ride
  class ApplicationController < ActionController::Base

    layout 'ride/application'

    # Automatically set the current user variable
    before_filter do

      #debugger

      #@current_user = (current_user) ? current_user : false
      
    end

    rescue_from CanCan::AccessDenied do |exception|
      
      #debugger

      if current_user
        flash[:error] = exception.message
        redirect_to(:controller => 'static', :action => 'access_denied') and return
      else
        flash[:error] = exception.message
        redirect_to(new_user_session_path) and return
      end
      
      #redirect_to(:controller => 'static', :action => 'access_denied') and return

    end

    def current_ability
      @current_ability ||= Ride::Ability.new current_user
    end    

    def paginate
      true
    end

  end
end
