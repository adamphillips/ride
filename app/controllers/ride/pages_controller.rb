module Ride

  class PagesController < Ride::Base::RideController

    load_and_authorize_resource :class => Ride::Page 

  end

end
