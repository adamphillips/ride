module Ride

  class EventsController < Ride::Base::RideController

    load_and_authorize_resource :class => Ride::Event

    def create

      params[:date] = Chronic.parse(params[:date]) if params[:date]

      super

    end

    def update

      params[:date] = Chronic.parse(params[:date]) if params[:date]

      super

    end

  end

end
