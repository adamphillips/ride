module Ride
  class DevelopmentMailInterceptor
    def self.delivering_email(message)
      message.subject = "[#{Rails.env.to_s}:#{message.to}] #{message.subject}"
      message.to = "adam@29ways.co.uk"
    end
  end
end
