Time::DATE_FORMATS[:short] = lambda { |time| time.strftime("%I:%M#{time.strftime("%p").downcase} #{time.day.ordinalize} %B")}
Time::DATE_FORMATS[:extra_long] = lambda { |time| time.strftime("%A the #{time.day.ordinalize} of %B at %I:%M#{time.strftime("%p").downcase}")}

