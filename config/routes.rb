Ride::Engine.routes.draw do

  devise_for :users, :class_name => "Ride::User", :controllers => {:sessions => 'ride/sessions'}

  resources :users

  resources :home_page_posts
  resources :home_page_images
  resources :enquiries
  resources :events
  resources :pages
  resources :products
  resources :media_links

  devise_scope :user do
    get 'sign_in' => 'sessions#new'
  end

  root :to => 'static#home'

end
