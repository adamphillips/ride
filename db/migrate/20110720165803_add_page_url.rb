class AddPageUrl < ActiveRecord::Migration
  def up
    add_column :ride_pages, :url, :string
  end

  def down
    drop_column :ride_pages, :url
  end
end
