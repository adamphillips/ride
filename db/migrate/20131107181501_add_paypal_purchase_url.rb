class AddPaypalPurchaseUrl < ActiveRecord::Migration
  def up
    add_column :ride_products, :paypal_purchase_url, :string
  end

  def down
    drop_column :ride_products, :paypal_purchase_url
  end
end
