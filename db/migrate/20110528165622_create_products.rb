class CreateProducts < ActiveRecord::Migration
  def up

    create_table(:ride_products) do |t|

      t.string :title
      t.string :type
      t.text   :description

      t.string :purchase_url

      t.string :image_file_name
      t.string :image_content_type
      t.integer :image_file_size
      t.datetime :image_updated_at

      t.timestamps

    end

  end

  def down
  end
end
