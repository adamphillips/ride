class CreateRidePages < ActiveRecord::Migration
  def self.up

    create_table(:ride_pages) do |t|

      t.string    :title
      t.string    :type
      t.text      :description
      
      t.timestamps

    end

    #add_index :ride_pages, :start_at
    #add_index :ride_pages, :display

  end

  def self.down
    drop_table :ride_pages
  end
end

