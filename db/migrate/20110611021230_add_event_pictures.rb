class AddEventPictures < ActiveRecord::Migration
  def up

    add_column :ride_events, :image_file_name, :string
    add_column :ride_events, :image_content_type, :string    
    add_column :ride_events, :image_file_size, :integer
    add_column :ride_events, :image_updated_at, :datetime

  end

  def down

    drop_column :ride_events, :image_file_name
    drop_column :ride_events, :image_content_type    
    drop_column :ride_events, :image_file_size
    drop_column :ride_events, :image_updated_at

  end
end
