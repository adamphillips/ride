class AddStorePurchaseUrls < ActiveRecord::Migration
  def up
    rename_column :ride_products, :purchase_url, :amazon_purchase_url
    add_column :ride_products, :itunes_purchase_url, :string
  end

  def down
    rename_column :ride_products, :amazon_purchase_url, :purchase_url
    drop_column :ride_products, :itunes_purchase_url
  end
end
