class CreateHomePageImages < ActiveRecord::Migration
  def up
      
    create_table(:ride_home_page_images) do |t|

      t.string :title
     
      t.string :image_file_name
      t.string :image_content_type
      t.integer :image_file_size
      t.datetime :image_updated_at

      t.timestamps

    end

  end

  def down

    drop_table :ride_home_page_images

  end
end
