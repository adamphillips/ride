class CreateEnquiries < ActiveRecord::Migration
  def up

   create_table(:ride_enquiries) do |t|

      t.string :name
      t.string :email
      t.text   :message
      t.string :i_tunes_code

      t.timestamps

   end

  end

  def down

    drop_table :ride_users

  end
end
