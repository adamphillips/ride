class AddCdBabyPurchaseUrl < ActiveRecord::Migration
  def up
    add_column :ride_products, :cdbaby_purchase_url, :string
  end

  def down
    drop_column :ride_products, :cdbaby_purchase_url
  end
end
