class CreateHomePagePosts < ActiveRecord::Migration
  def up

    create_table :ride_home_page_posts do |t|

      t.string :title
      t.text :content

      t.timestamps

    end

  end

  def down

    drop_table :ride_home_page_posts

  end
end
