class CreateRideModels < ActiveRecord::Migration
  def self.up

    create_table(:ride_models) do |t|

      t.string :type

      t.timestamps

    end

  end

  def self.down
    drop_table :ride_models
  end
end

