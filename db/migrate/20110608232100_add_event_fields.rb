class AddEventFields < ActiveRecord::Migration
  def up
    add_column :ride_events, :date, :datetime
    add_column :ride_events, :address, :text
  end

  def down
    drop_column :ride_events, :date
    drop_column :ride_events, :address
  end
end
