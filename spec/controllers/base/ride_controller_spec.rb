require 'spec_helper'

describe Ride::Base::RideController do

  before :each do

    module Ride
      class ObjectsController < Ride::Base::RideController
      end
    end

  end

  describe 'Class Methods' do

    describe '#model_name' do

      it 'should return the name of the model associated with the controller' do

        Ride::ObjectsController.model_name.should == 'Ride::Object'

      end

    end

    describe '#plural_variable_name' do

      it 'should return the associated plural variable name' do

        Ride::ObjectsController.plural_variable_name.should == 'objects'  

      end

    end

    describe '#singular_variable_name' do

      it 'should return the associated singular variable name' do

        Ride::ObjectsController.singular_variable_name.should == 'object'  

      end

    end

  end

end
