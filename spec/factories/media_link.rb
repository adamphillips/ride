Factory.define :ride__media_link, :class => Ride::MediaLink do end

Factory.define :ride__media_links__you_tube_video, :class => Ride::MediaLinks::YouTubeVideo do end
Factory.define :ride__media_links__sound_cloud_player, :class => Ride::MediaLinks::SoundCloudPlayer do end
Factory.define :ride__media_links__photo, :class => Ride::MediaLinks::Photo do end
