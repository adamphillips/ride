Ride.admin_models = {

  'Ride::HomePagePost' => Ride::HomePagePost,
  'Ride::HomePageImage' => Ride::HomePageImage,
  'Ride::Page' => Ride::Page,
  'Ride::Event' => Ride::Event,
  'Ride::MediaLink' => Ride::MediaLink,
  'Ride::Product' => Ride::Product,
  'Ride::Enquiry' => Ride::Enquiry,

}

Ride.admin_title = 'Ride Test Site'
Ride.notification_email = 'adam@29ways.co.uk'
