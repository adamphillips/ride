Rails.application.routes.draw do

  mount Ride::Engine => "/ride"

  #root :to => 'ride/static#home'
  
  get 'contact' => 'static#contact'
  post 'contact' => 'static#contact_submitted'

end
