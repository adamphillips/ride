class StaticController < ApplicationController

  def contact

    @enquiry = Ride::Enquiry.new

  end
  
  def contact_submitted

    @enquiry = Ride::Enquiry.new(params[:enquiry]);

    @enquiry.save    

    Ride::EnquiryMailer.notify(@enquiry).deliver

  end

end
