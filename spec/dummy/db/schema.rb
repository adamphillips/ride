# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131107181501) do

  create_table "ride_enquiries", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.text     "message"
    t.string   "i_tunes_code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ride_events", :force => true do |t|
    t.string   "title"
    t.string   "type"
    t.text     "description"
    t.datetime "start_at"
    t.boolean  "display",            :default => false
    t.text     "location"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "date"
    t.text     "address"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "ride_events", ["display"], :name => "index_ride_events_on_display"
  add_index "ride_events", ["start_at"], :name => "index_ride_events_on_start_at"

  create_table "ride_home_page_images", :force => true do |t|
    t.string   "title"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ride_home_page_posts", :force => true do |t|
    t.string   "title"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ride_media_links", :force => true do |t|
    t.string   "title"
    t.string   "type"
    t.text     "description"
    t.string   "url"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ride_models", :force => true do |t|
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ride_pages", :force => true do |t|
    t.string   "title"
    t.string   "type"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "url"
  end

  create_table "ride_products", :force => true do |t|
    t.string   "title"
    t.string   "type"
    t.text     "description"
    t.string   "amazon_purchase_url"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "itunes_purchase_url"
    t.string   "cdbaby_purchase_url"
    t.string   "paypal_purchase_url"
  end

  create_table "ride_users", :force => true do |t|
    t.string   "email",                                 :default => "", :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ride_users", ["email"], :name => "index_ride_users_on_email", :unique => true
  add_index "ride_users", ["reset_password_token"], :name => "index_ride_users_on_reset_password_token", :unique => true

end
