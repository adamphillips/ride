require 'spec_helper'

describe Ride::Base::Model do

  describe '#ride' do

    before :each do

      #debugger

      @test = Ride::Base::Model

      class TestSubClass < Ride::Base::Model
      end

      @sub_test = TestSubClass

    end

    it 'should be defined on Ride::Base::Model' do

      #debugger

      @test.respond_to?(:ride).should == true

    end

    describe 'when called' do

      context 'given the :display_in_admin option is not specified' do

        it 'should not add it to the Ride.admin_models list' do

          num_models = Ride.admin_models.length

          @test.ride 

          #debugger

          Ride.admin_models.length.should == num_models

          Ride.admin_models.include?(@test).should == false

        end

      end

      context 'given the :display_in_admin option is set to false' do

        it 'should not add it to the Ride.admin_models list' do

          num_models = Ride.admin_models.length
          
          @test.ride :display_in_admin => false

          #debugger

          Ride.admin_models.length.should == num_models

          Ride.admin_models.include?(@test).should == false

        end

      end

      context 'given the :display_in_admin option is set to true' do

        it 'should add it to the Ride.admin_models list' do

          num_models = Ride.admin_models.length
          
          #debugger

          @test.ride :display_in_admin => true

          #debugger

          Ride.admin_models.length.should == num_models + 1

          Ride.admin_models.include?(@test.name).should == true

        end

      end

      context 'given no :table parameter is passed' do

        it 'should set the model\'s table to the name of the model and prepend the ride_ prefix' do

          @sub_test.ride

          @sub_test.table_name.should == 'ride_test_sub_classes'

        end

      end

      context 'given a specific table is passed in the :table parameter' do

        it 'should set the model\'s table to the specified table' do

          @sub_test.ride :table => 'another_table'

          @sub_test.table_name.should == 'another_table'

        end

      end

    end

  end

end
