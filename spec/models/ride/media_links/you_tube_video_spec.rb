require 'spec_helper'

describe Ride::MediaLinks::YouTubeVideo do

  before :each do
    @media_link = Factory.build(:ride__media_links__you_tube_video)
  end

   describe '#type_class' do
    it 'should return "video"' do
      @media_link.type_class.should == 'video'
    end
  end

  describe '#type_title' do
    it 'should return "YouTube video"' do
      @media_link.type_title.should == 'Video'
    end
  end

end
