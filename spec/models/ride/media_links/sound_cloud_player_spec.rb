require 'spec_helper'

describe Ride::MediaLinks::SoundCloudPlayer do

  before :each do
    @media_link = Factory.build(:ride__media_links__sound_cloud_player)
  end

   describe '#type_class' do
    it 'should return "music"' do
      @media_link.type_class.should == 'music'
    end
  end

  describe '#type_title' do
    it 'should return "SoundCloud song"' do
      @media_link.type_title.should == 'Music'
    end
  end

end

