require 'spec_helper'

describe Ride::MediaLinks::Photo do

  before :each do

    @media_link = Factory.build(:ride__media_links__photo)

  end

   describe '#type_class' do

    it 'should return "photo"' do

      @media_link.type_class.should == 'photo'

    end

  end

  describe '#type_title' do

    it 'should return "Photo"' do

      @media_link.type_title.should == 'Photo'

    end

  end

end


