require 'spec_helper'

describe Ride::Enquiry do

  describe '#title' do

    before :each do

      @enquiry = Factory(:ride__enquiry, :created_at => DateTime.parse('7pm 7th may 2011'), :name => 'Adam', :email => 'adam@29ways.co.uk')

    end

    it 'should return the date and name combined' do

      @enquiry.title.should == 'Adam - May 7th, 2011'

    end

  end

end
