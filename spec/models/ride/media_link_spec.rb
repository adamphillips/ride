require 'spec_helper'

describe Ride::MediaLink do
  describe '#type_title' do
    it 'should return a humanized form of the media link without attached name space' do
      ml = Factory(:ride__media_link)
      ml.type_title.should == 'Media link'
    end
  end
end
