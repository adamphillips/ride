# Provide a simple gemspec so you can easily use your
# project in your rails apps through git.
Gem::Specification.new do |s|
  s.name = 'ride'
  s.summary = 'Ride CMS'
  s.description = 'CMS for band websites'
  s.files = Dir["lib/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.version = '0.0.2'
  s.authors = ['Adam Phillips']


  s.add_dependency 'devise'
  s.add_dependency 'cancan'
  s.add_dependency 'aws-sdk'
  s.add_dependency 'paperclip'
  s.add_dependency 'chronic'
  s.add_dependency 'will_paginate', '3.0.pre4'

  s.add_development_dependency 'rspec-rails', '~> 2.6'
  s.add_development_dependency 'capybara', '~> 0.4'
  s.add_development_dependency 'factory_girl', '~> 1.3'
end
