# desc "Explaining what the task does"
# task :ride do
#   # Task goes here
# end

namespace :ride do

  task :create_user => :environment do

    desc 'Creates a new user'

    puts "\n\n*****************************"
    puts "*  Create a new Ride user   *"
    puts "*****************************"
    puts "\n\nPlease enter the email for the new user:"

    email = STDIN.gets.strip

    puts "\nPlease enter a password for the new user:"

    password = STDIN.gets.strip

    u = Ride::User.new(:email => email)

    u.password = password

    #debugger

    if u.save

      puts "\n\nUser created"

    else

      puts "\n\nCould not create user"

    end

  end

  desc "Show the date/time format strings defined and example output"
  task :date_formats => :environment do

    now = Time.now

    [:to_date, :to_datetime, :to_time].each do |conv_meth|
      
      obj = now.send(conv_meth)
      
      puts obj.class.name
      puts "=" * obj.class.name.length
      
      name_and_fmts = obj.class::DATE_FORMATS.map { |k, v| [k, %Q('#{String === v ? v : '&proc'}')] }
      max_name_size = name_and_fmts.map { |k, _| k.to_s.length }.max + 2
      max_fmt_size = name_and_fmts.map { |_, v| v.length }.max + 1
      
      name_and_fmts.each do |format_name, format_str|
        puts sprintf("%#{max_name_size}s:%-#{max_fmt_size}s %s", format_name, format_str, obj.to_s(format_name))
      end
      
      puts

    end
  end

end
