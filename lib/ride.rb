require 'active_record'
require 'cancan'
require 'devise'
require 'will_paginate'
require "ride/engine"

module Ride

    @@admin_models = {}
    @@admin_title = ''
    @@notification_email = 'adam@29ways.co.uk'

    def self.admin_models
      @@admin_models
    end

    def self.admin_models= (models)
      @@admin_models = models
    end

    def self.admin_title
      @@admin_title
    end

    def self.admin_title= (title)
      @@admin_title = title
    end

    def self.notification_email
      @@notification_email
    end

    def self.notification_email= (notification_email)
      @@notification_email = notification_email
    end


end
