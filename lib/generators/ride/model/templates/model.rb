module Ride
  class <%= name.classify %> < Ride::Base::Model

    <% if :display_in_admin == true %>
    ride :display_in_admin => true
    <% else %>
    ride :display_in_admin => false
    <% end %>

  end
end
