module Ride
  class ModelGenerator < Rails::Generators::Base

    source_root File.expand_path('../templates', __FILE__)
    argument :name, :type => :string, :required => true, :description => 'The name of the model'
    class_option :display_in_admin, :type => :boolean, :required => false, :default => true, :description => 'Wether to display this model in the ride adminstration area'

    def create_model_file
      template 'model.rb', 'app/models/ride/' + name.singularize.underscore + '.rb'
    end

    def create_rspec_file
      template 'model_spec.rb', 'spec/models/ride/' + name.singularize.underscore + '_spec.rb'
    end

  end
end


