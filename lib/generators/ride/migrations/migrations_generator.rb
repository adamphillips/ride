require 'rails/generators'
require 'rails/generators/migration'
 
module Ride
  module Generators
   class MigrationsGenerator < Rails::Generators::Base

    include Rails::Generators::Migration

    source_root File.expand_path("../templates", __FILE__)

    desc 'Creates default migrations for installing Ride'

    def self.next_migration_number(dirname)

        @@current_migration_count ||= 0

        if ActiveRecord::Base.timestamped_migrations
            ret = Time.new.utc.strftime("%Y%m%d%H%M%S") + '001'
        else
            ret = "%.3d" % (current_migration_number(dirname) + 1)
        end

        ret = ret.to_i + @@current_migration_count

        @@current_migration_count = @@current_migration_count + 1

        ret
    end
   
    def create_migration_file
        
        #migration_template 'migration.rb', 'db/migrate/create_pages_table.rb'
        
        #debugger

        Dir[File.join(File.dirname(__FILE__), "templates", "**")].each {|f| migration_template f, 'db/migrate/' + File.basename(f) }
    end
   end
 end
end
