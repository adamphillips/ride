class CreateRideEvents < ActiveRecord::Migration
  def self.up

    create_table(:ride_events) do |t|

      t.string    :title
      t.string    :type
      t.text      :description
      t.datetime  :start_at
      t.boolean   :display,     :default => false
      t.text      :location

      t.timestamps

    end

    add_index :ride_events, :start_at
    add_index :ride_events, :display

  end

  def self.down
    drop_table :ride_events
  end
end
