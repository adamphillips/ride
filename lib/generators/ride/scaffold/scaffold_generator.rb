module Ride
  class ScaffoldGenerator < Rails::Generators::Base

    source_root File.expand_path('../templates', __FILE__)
    argument :name, :type => :string, :required => true, :description => 'The name of the model to generate the scaffold for'
    class_option :admin_only, :type => :boolean, :required => false, :default => false, :description => 'Wether to require admin authentication for the controller'
    class_option :display_in_admin, :type => :boolean, :required => false, :default => false, :description => 'Wether to display this model in the ride adminstration area'
  
    def create_controller

      param_str = name
      param_str += ' --admin-only' if :admin_only == true

      generate 'ride:controller', param_str

    end

    def create_migration

      generate 'migration', 'Create' + name.classify.pluralize

    end

    def create_model

      param_str = name
      param_str += ' --display-in-admin' if :display_in_admin == true

      generate 'ride:model', param_str

    end

    def create_views

      param_str = name

      generate 'ride:views', param_str

    end


  end
end

