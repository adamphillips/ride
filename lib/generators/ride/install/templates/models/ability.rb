class Ability < Ride::Ability

  def initialize(user)

    user ||= Ride::User.new

    @user = user

    super user

  end

end

