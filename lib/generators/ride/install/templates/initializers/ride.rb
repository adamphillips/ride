Ride.admin_models = {

  'Ride::Page' => Ride::Page,
  'Ride::Event' => Ride::Event,
  'Ride::MediaLink' => Ride::MediaLink,
  'Ride::Product' => Ride::Product,
  'Ride::Enquiry' => Ride::Enquiry,

}

Ride.admin_title = 'Ride Default Site'
Ride.notification_email = 'adam@29ways.co.uk'

