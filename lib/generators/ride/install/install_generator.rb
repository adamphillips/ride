require 'rails/generators'
 
module Ride
  module Generators
   class InstallGenerator < Rails::Generators::Base

    include Rails::Generators::Migration

    source_root File.expand_path("../templates", __FILE__)

    desc 'Creates default migrations for installing Ride'

    def copy_ability
      template 'models/ability.rb', 'app/models/ability.rb'
    end
    
    def copy_initializer
      template 'initializers/ride.rb', 'config/initializers/ride.rb'
    end
    
    def copy_stylesheet
      template 'stylesheets/ride.css', 'app/assets/stylesheets/ride.css'
    end

    def create_user

      if User.all.length == 0

        #task 'ride:create_user'

      end

    end

   end
 end
end
