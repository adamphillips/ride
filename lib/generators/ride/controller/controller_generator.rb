module Ride
  class ControllerGenerator < Rails::Generators::Base

    source_root File.expand_path('../templates', __FILE__)
    argument :name, :type => :string, :required => true, :description => 'The name of the controller'
    class_option :admin_only, :type => :boolean, :required => false, :default => false, :description => 'If true, the controller is only accessible to authenticated users'

    def create_controller_file
      template 'controller.rb', 'app/controllers/ride/' + name.pluralize.underscore + '_controller.rb'
    end

    def create_rspec_file
      template 'controller_spec.rb', 'spec/controllers/ride/' + name.pluralize.underscore + '_controller_spec.rb'
    end

  end
end
