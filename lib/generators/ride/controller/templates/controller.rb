module Ride
  class <%= name.classify.pluralize + 'Controller' %> < Ride::Base::RideController

    <% if :admin_only == true %>
      load_and_authorize_resource :class => Ride::Base::Model
    <% end %>

  end
end
