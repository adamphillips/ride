module Ride
  class ViewsGenerator < Rails::Generators::Base

    source_root File.expand_path('../templates', __FILE__)
    argument :name, :type => :string, :required => true, :description => 'The name of the model related to the views'

    def create_views
      template '_form.html.erb', 'app/views/ride/' + name.pluralize.underscore + '/_form.html.erb'
      template '_small.html.erb', 'app/views/ride/' + name.pluralize.underscore + '/_small.html.erb'

      template 'edit.html.erb', 'app/views/ride/' + name.pluralize.underscore + '/edit.html.erb'
      template 'index.html.erb', 'app/views/ride/' + name.pluralize.underscore + '/index.html.erb'
      template 'new.html.erb', 'app/views/ride/' + name.pluralize.underscore + '/new.html.erb'
      template 'show.html.erb', 'app/views/ride/' + name.pluralize.underscore + '/show.html.erb'
    end

  end
end



