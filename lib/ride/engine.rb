module Ride
  class Engine < Rails::Engine

    isolate_namespace Ride

    config.ride = Ride

    # Force routes to be loaded if we are doing any eager load.
    config.before_eager_load { |app| app.reload_routes! }

    #initializer "ride.url_helpers" do
      #Ride.include_helpers(Ride::Controllers)
    #end
    
    initializer 'ride.load_model_helpers', :after => :disable_dependency_loading do |app|

      ActiveRecord::Base.send(:include, Ride::ModelsHelper)      

    end

    initializer 'ride.configure_devise', :after => :disable_dependency_loading do |app|

      #puts 'Ride is loading'

    end

    #initializer 'ride.load_will_paginate', :after => :disable_dependency_loading do |app|

      #require 'will_paginate'

    #end

  end
end
